static const char *RcsId = "$Header: /segfs/tango/cppserver/temperature/eurotherm2400/RCS/Eurotherm2400.cpp,v 1.1 2009/04/29 15:23:29 witsch Exp $";
//+=============================================================================
//
// file :         Eurotherm2400.cpp
//
// description :  C++ source for the Eurotherm2400 and its commands. 
//                The class is derived from Device. It represents the
//                CORBA servant object which will be accessed from the
//                network. All commands which can be executed on the
//                Eurotherm2400 are implemented in this file.
//
// project :      TANGO Device Server
//
// $Author: witsch $
//
// $Revision: 1.1 $
//
// $Log: Eurotherm2400.cpp,v $
// Revision 1.1  2009/04/29 15:23:29  witsch
// Initial revision
//
// Revision 1.1  2009/04/29 15:18:34  witsch
// Initial revision
//
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================



//===================================================================
//
//	The following table gives the correspondence
//	between commands and method name.
//
//  Command name|  Method name
//	----------------------------------------
//  State   |  dev_state()
//  Status  |  dev_status()
//
//===================================================================


#include <tango.h>
#include <Eurotherm2400.h>
#include <Eurotherm2400Class.h>

namespace Eurotherm2400_ns
{

//+----------------------------------------------------------------------------
//
// method : 		Eurotherm2400::Eurotherm2400(string &s)
// 
// description : 	constructor for simulated Eurotherm2400
//
// in : - cl : Pointer to the DeviceClass object
//      - s : Device name 
//
//-----------------------------------------------------------------------------
Eurotherm2400::Eurotherm2400(Tango::DeviceClass *cl,string &s)
:Modbus_ns::Modbus(cl,s.c_str())
{
	init_device();
  device_constructed = true;
}

Eurotherm2400::Eurotherm2400(Tango::DeviceClass *cl,const char *s)
:Modbus_ns::Modbus(cl,s)
{
	init_device();
  device_constructed = true;
}

Eurotherm2400::Eurotherm2400(Tango::DeviceClass *cl,const char *s,const char *d)
:Modbus_ns::Modbus(cl,s,d)
{
	init_device();
  device_constructed = true;
}
//+----------------------------------------------------------------------------
//
// method : 		Eurotherm2400::delete_device()
// 
// description : 	will be called at device destruction or at init command.
//
//-----------------------------------------------------------------------------
void Eurotherm2400::delete_device()
{
	//	Delete device's allocated object
	if (device_constructed == true)
		Modbus_ns::Modbus::delete_device();
}


//+----------------------------------------------------------------------------
//
// method : 		Eurotherm2400::init_device()
// 
// description : 	will be called at device initialization.
//
//-----------------------------------------------------------------------------
void Eurotherm2400::init_device()
{
	string infostr;
	INFO_STREAM << "Eurotherm2400::Eurotherm2400() create device " << device_name << endl;

	// Initialise variables to default values
	//--------------------------------------------
	get_device_property();
	modbus = NULL;
  if (device_constructed == true)
		Modbus_ns::Modbus::init_device();
  try {
	  modbus = new Tango::DeviceProxy(modbusDevice);
	  set_state(Tango::RUNNING); // State set to ON at connect time.
    infostr = "Connected to device: ";
    infostr.append(modbusDevice);
    cout << infostr << endl;
	  set_status(infostr);
	} 
	catch (Tango::DevFailed &e) {
    set_state(Tango::FAULT);
    infostr = "Unable to connect to device: ";
    infostr.append(modbusDevice);
    cout << infostr << endl;
    set_status(infostr);
  }
}


//+----------------------------------------------------------------------------
//
// method : 		Eurotherm2400::get_device_property()
// 
// description : 	Read the device properties from database.
//
//-----------------------------------------------------------------------------
void Eurotherm2400::get_device_property()
{
	//	Initialize your default values here (if not done with  POGO).
	//------------------------------------------------------------------

	//	Read device properties from database.(Automatic code generation)
	//------------------------------------------------------------------
	Tango::DbData	dev_prop;
	dev_prop.push_back(Tango::DbDatum("ModbusDevice"));

	//	Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
		get_db_device()->get_property(dev_prop);
	Tango::DbDatum	def_prop, cl_prop;
	Eurotherm2400Class	*ds_class =
		(static_cast<Eurotherm2400Class *>(get_device_class()));
	int	i = -1;

	//	Try to initialize ModbusDevice from class property
	cl_prop = ds_class->get_class_property(dev_prop[++i].name);
	if (cl_prop.is_empty()==false)	cl_prop  >>  modbusDevice;
	else {
		//	Try to initialize ModbusDevice from default device value
		def_prop = ds_class->get_default_device_property(dev_prop[i].name);
		if (def_prop.is_empty()==false)	def_prop  >>  modbusDevice;
	}
	//	And try to extract ModbusDevice value from database
	if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  modbusDevice;



	//	End of Automatic code generation
	//------------------------------------------------------------------

}
//+----------------------------------------------------------------------------
//
// method : 		Eurotherm2400::always_executed_hook()
// 
// description : 	method always executed before any command is executed
//
//-----------------------------------------------------------------------------
void Eurotherm2400::always_executed_hook()
{
  Modbus_ns::Modbus::always_executed_hook();
}
//+----------------------------------------------------------------------------
//
// method : 		Eurotherm2400::read_attr_hardware
// 
// description : 	Hardware acquisition for attributes.
//
//-----------------------------------------------------------------------------
void Eurotherm2400::read_attr_hardware(vector<long> &attr_list)
{
	DEBUG_STREAM << "Eurotherm2400::read_attr_hardware(vector<long> &attr_list) entering... "<< endl;
	//	Add your own code here
  Modbus_ns::Modbus::read_attr_hardware(attr_list);
}

//+----------------------------------------------------------------------------
//
// method : 		Eurotherm2400::float_from()
// 
// description : 	Create a IEEE float from two shorts
//
//-----------------------------------------------------------------------------
#include <math.h>
double float_from(unsigned short sh0, unsigned short sh1) {
  // DEBUG_STREAM << "Eurotherm2400::float_from(): Eurotherm IEEE float conversion"<< endl;
//  cout << "Eurotherm2400::float_from(): " <<sh0<< " "  <<sh1;
	unsigned char   s[4];
  unsigned long   f1;
  double  x;
  int     e;
  s[0] = sh0 >> 8 & 0xff; s[1] = sh0 & 0xff;
  s[2] = sh1 >> 8 & 0xff; s[3] = sh1 & 0xff;
  f1 = 0x800000 | (s[1]&0x7f) << 16 | s[2] << 8 | s[3];
  e = ((s[0]&0x7F)<<1) | ((s[1]&0x80)>>7);
  e -= 127;
  x = f1 * pow(2., -23. + e);
  if (s[0]&0x80)
          x = -x;
//  cout <<" to " << x << endl;
  return(x);
}


//+----------------------------------------------------------------------------
//
// method : 		Eurotherm2400::read_Temperature
// 
// description : 	Extract real attribute values for Temperature acquisition result.
//
//-----------------------------------------------------------------------------
void Eurotherm2400::read_Temperature(Tango::Attribute &attr)
{
	unsigned short   s[2];
	DEBUG_STREAM << "Eurotherm2400::read_Temperature(Tango::Attribute &attr) entering... "<< endl;
  try {
		Tango::DeviceData outArg;
		Tango::DeviceData inArg;
		Tango::DevVarShortArray *paramIn  = new Tango::DevVarShortArray();
		const Tango::DevVarShortArray *paramOut;
    double reading;

		paramIn->length(2);
		(*paramIn)[0] = 0x8002;
		(*paramIn)[1] = 2;
// commented lines to read the integer from Modbus address 1
//		(*paramIn)[0] = 1;
//		(*paramIn)[1] = 1;
		inArg << paramIn;

		outArg = modbus->command_inout("ReadInputRegisters", inArg);
		outArg >> paramOut;

	  DEBUG_STREAM << "Eurotherm2400::read_Temperature() is " << hex <<
    (*paramOut)[0] << " " << (*paramOut)[1] << endl;

//		attr_Temperature_read = (*paramOut)[0];
 		reading = float_from((*paramOut)[0], (*paramOut)[1]);
	  DEBUG_STREAM << "Eurotherm2400::read_Temperature() is " << reading << endl;
 		attr_Temperature_read = reading;
	}
	catch (Tango::DevFailed &f)	{
    DEBUG_STREAM << "Exception" << endl;
		set_status("Unable to read temperature.");
    throw f;
	}
	attr.set_value(&attr_Temperature_read); 
}




//+------------------------------------------------------------------
/**
 *	method:	Eurotherm2400::dev_state
 *
 *	description:	method to execute "State"
 *	This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
 *
 * @return	State Code
 *
 */
//+------------------------------------------------------------------
Tango::DevState Eurotherm2400::dev_state()
{
	Tango::DevState	argout = DeviceImpl::dev_state();
	DEBUG_STREAM << "Eurotherm2400::dev_state(): entering... !" << endl;

	//	Add your own code to control device here

	set_state(argout);
	return argout;
}



}	//	namespace
