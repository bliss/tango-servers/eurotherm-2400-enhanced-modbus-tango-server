# Eurotherm 2400 enhanced modbus tango server

The server works well with the SPEC macros eurotherm2400.mac. 
It is essentially a modbus tcp server, but does an additional polling of the readout value, which can then be read very quickly. 

Perhaps would it be beneficial, to poll other values, like all the ones used in E2400_cmd().

Use: make linux=1

The orignal place was /segfs/tango/cppserver/temperature/eurotherm2400.


The old README file was :

Compiled for ubuntu2004:  16 November 2021

Last attempt to recompile was 23/5/2018.
    - the Makefile has been modified to allow for linuxmake to run through, no matter the OS.

This is old (23/5/2018):
Last attempt to recompile were undertaken on 12 April 2016. Redhate4 and debian7 worked, debian6 failed and I have no system for compiling on redhate5.




17/11/2021:
    make will do if started as:
        make linux=1
    Also the Makefile was changed to accomodate the changes in the include file structure. Tango.h is now one level deeper in subdir tango.
    to date the library log4tango is missing.
