//=============================================================================
//
// file :        Eurotherm2400.h
//
// description : Include for the Eurotherm2400 class.
//
// project :	eurotherm2400
//
// $Author: witsch $
//
// $Revision: 1.1 $
//
// $Log: Eurotherm2400.h,v $
// Revision 1.1  2009/04/29 15:23:29  witsch
// Initial revision
//
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================
#ifndef _EUROTHERM2400_H
#define _EUROTHERM2400_H

#include <tango.h>
#include <Modbus.h>

//using namespace Tango;

/**
 * @author	$Author: witsch $
 * @version	$Revision: 1.1 $
 */

 //	Add your own constants definitions here.
 //-----------------------------------------------


namespace Eurotherm2400_ns
{

/**
 * Class Description:
 * tango server to control the eurotherm temperature controller series 2400
 */

/*
 *	Device States Description:
*  Tango::RUNNING :  Is the server running ?
*  Tango::FAULT :    Something didn't work!
 */


class Eurotherm2400: public Modbus_ns::Modbus
{
public :
	//	Add your own data members here
	//-----------------------------------------

	//	Here is the Start of the automatic code generation part
	//-------------------------------------------------------------	
/**
 *	@name attributes
 *	Attributs member data.
 */
//@{
		Tango::DevDouble	attr_Temperature_read;
//@}

/**
 *	@name Device properties
 *	Device properties member data.
 */
//@{
/**
 *	name of the modbus tango device
 */
	string	modbusDevice;
//@}

/**@name Constructors
 * Miscellaneous constructors */
//@{
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	Eurotherm2400(Tango::DeviceClass *cl,string &s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	Eurotherm2400(Tango::DeviceClass *cl,const char *s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device name
 *	@param d	Device description.
 */
	Eurotherm2400(Tango::DeviceClass *cl,const char *s,const char *d);
//@}

/**@name Destructor
 * Only one desctructor is defined for this class */
//@{
/**
 * The object desctructor.
 */	
	~Eurotherm2400() {
    device_constructed = false;
    delete_device();
  };
/**
 *	will be called at device destruction or at init command.
 */
	void delete_device();
//@}

	
/**@name Miscellaneous methods */
//@{
/**
 *	Initialize the device
 */
	virtual void init_device();
/**
 *	Always executed method befor execution command method.
 */
	virtual void always_executed_hook();

//@}

/**
 * @name Eurotherm2400 methods prototypes
 */

//@{
/**
 *	Hardware acquisition for attributes.
 */
	virtual void read_attr_hardware(vector<long> &attr_list);
/**
 *	Extract real attribute values for Temperature acquisition result.
 */
	virtual void read_Temperature(Tango::Attribute &attr);
/**
 *	Read/Write allowed for Temperature attribute.
 */
	virtual bool is_Temperature_allowed(Tango::AttReqType type);
/**
 * This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
 *	@return	State Code
 *	@exception DevFailed
 */
	virtual Tango::DevState	dev_state();

/**
 *	Read the device properties from database
 */
	 void get_device_property();
//@}

	//	Here is the end of the automatic code generation part
	//-------------------------------------------------------------	

protected :	
	//	Add your own data members here
	//-----------------------------------------
	Tango::DeviceProxy * modbus;
  bool device_constructed;
};

}	// namespace_ns

#endif	// _EUROTHERM2400_H
