static const char *RcsId = "$Header: /segfs/tango/cppserver/temperature/eurotherm2400/RCS/ClassFactory.cpp,v 1.1 2009/04/29 15:20:23 witsch Exp $";
//+=============================================================================
//
// file :        ClassFactory.cpp
//
// description : C++ source for the class_factory method of the DServer
//               device class. This method is responsible to create
//               all class singletin for a device server. It is called
//               at device server startup
//
// project :     TANGO Device Server
//
// $Author: witsch $
//
// $Revision: 1.1 $
//
// $Log: ClassFactory.cpp,v $
// Revision 1.1  2009/04/29 15:20:23  witsch
// Initial revision
//
// Revision 1.1  2009/04/29 15:18:34  witsch
// Initial revision
//
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================


#include <tango.h>
#include <SerialClass.h>
#include <ModbusClass.h>
#include <Eurotherm2400Class.h>

/**
 *	Create Eurotherm2400Class singleton and store it in DServer object.
 */

void Tango::DServer::class_factory()
{

  add_class(Serial_ns::SerialClass::init("Serial"));
  add_class(Modbus_ns::ModbusClass::init("Modbus"));
	add_class(Eurotherm2400_ns::Eurotherm2400Class::init("Eurotherm2400"));

}
